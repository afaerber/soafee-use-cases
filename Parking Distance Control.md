# Description
On the platform multiple distance sensors (RADAR/LiDAR/ultrasonic at different locations&angles) provide distance data, and sound can be emitted through IP-based commands to another controller.
The user (driver) expects timely audible feedback via loudspeakers, with beep frequency depending on the distance.

## Normalization

### Time-sensitive network communication into and out of containers
*Description* 
A containerized application may have time constraints for receiving and processing incoming data packets, as well as time expectations for outgoing data packets to hit the physical medium.
In Kubernetes a suitable Container Network Interface (CNI) may need to be selected that can recognize and handle Ethernet TSN extensions in incoming and outgoing data packets.

### Scheduling and execution of real-time priority containers
*Description* 
The Orchestrator needs to be provided at deployment time with an indication from Developer whether a particular container must be scheduled on a node capable of real-time execution (e.g., Linux PREEMPT_RT) or whether a regular node is sufficient. If required by Developer and no such node is available to Orchestrator at deployment time, deployment must fail.
The Orchestrator must ensure at run-time that the container runtime always executes the container application process with real-time priority, if so configured at deployment time. It should not assign such real-time priority to container application processes that did not explicitly configure this.
The Orchestrator should allow for deployment of a pod consisting of containers where only some containers require real-time priority. Containers without real-time priority requirement may be scheduled on a node with real-time capabilities. If that is not desired, it is up to the Developer to configure the nodes and containers in a way that this does not happen.

- #1+
- [ ] #1+
- #2+

 

### Fail-over for critical containers
*Description*
Should a containerized application crash, the Orchestrator must make the container available again in a timely manner.
Will the Orchestrator re-starting the container after failure detection be fast enough?
Does the Developer need to schedule a backup pod/container in advance to accelerate the Orchestrator’s fail-over time?
