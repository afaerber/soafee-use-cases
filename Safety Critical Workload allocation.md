# Description

An application needs to allocate safety-critical workloads to suitable processing elements and configure certain safety properties of the processing elements.

The application must have the ability to discover safety attributes of the processing elements and allocate workloads to processing elements based on following processing element attributes:

Systematic ASIL  integrity1

guaranteed processor performance2

processing element FIT rate3

The application must have the ability to configure the diagnostic time interval4 of the processing element.

The application is expected to discover the list of safety-capable processing elements and calculate an allocation for each of its safety-critical workload by finding the best fit of workloads to safety-capable processing element using the following criteria:

Workload diagnostic time interval requirement is less than or equal to configured processing element diagnostic time interval

Workload diagnostic required is lower than or equal to processing element ASIL systematic integrity

Sum of required workload guaranteed processing per cycle of workloads allocated to a safety-capable processor is less than or equal to guaranteed processor performance multiplied by the diagnostic time interval

Workload maximum FIT rate is less than or equal to the processing element FIT rate time required workload guaranteed processing divided by guaranteed processor performance multiplied by the diagnostic time interval. 

[1] Systematic ASIL integrity is a static value input into a table based on hardware safety analysis of the platform.

[2] guaranteed processor performance is based on ACPI CPPC measurement definition and support or similar.

[3] processing element FIT rate is expected to be a static value input into a table from the hardware safety analysis of the platform.

## Normalization 

TO DO