# Description
The Driver Monitoring System is based on cameras to monitor driver alertness, The DMS service is decomposed  into DMS sub-services(face detect, eye movement, head movement, etc), and is provided by configuring the services pipeline of those DMS sub-services. The driver alert service should be audible feedback via speakers in expected time.

What’s LGE focusing?

Decomposition legacy monolithic service to containerized microservices. 

Making up an architecture to achieve vehicle scenario based on MSA

User Scenario Improvements based on Container Orchestration in SDV Requirements

Talking Point

Need to get communication methods between Vehicle Interface and Container Orchestrator

PICCOLO might be the methods, But now It supports only DBUS.

LGE would like to request that SOAFEE consider SOA frameworks such as DDS, SOME/IP commonly used in Vehicle.

.Need to get management solutions for nodes, computing resources, service list available to a vehicle.

PICCOLO manages extra nodes, services to achieve after market scenario on LGE PoC.

When SDV is achieved, user can purchase additional resources or services on demand after production line.

The management solutions should manage to prevent unauthorized users from using extra resources or services through Orchestrator.

Need to consider TSN for deployments on Mixed Critical Environments

PICCOLO used two attributes(affinity, metadata) for deployments to consider Device Accessibility, Safety Level, Realtime, etc.

LGE would like to request that SOAFEE find out a methods to schedule and to deploy services appropriately based on Mixed Critical Environments used TSN.

## Normalization

### Selection target work node to deploy
*Description*
The Orchestrator needs to be determined the location at deployment time with the affinity and capability according to the node capability. The Orchestraor should collect the latency information of each worker node. The Orchestrator should deploy pods where some containers require time criticality depend on latency.

### Deploy a new service
*Description*
After deployment, the operator should verify the request and the Orchestrator shoud configure the determined node connection. The Orchestraot adds the registry the deployed services.